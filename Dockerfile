FROM openjdk:11 as builder

COPY . .
RUN ./gradlew build

FROM registry.gitlab.com/crypto20/bitnami-docker-spark:3-1-1-j11

RUN mkdir /opt/applications
COPY --from=builder ./app/build/libs/app-all.jar /opt/applications/binance-spark.jar
