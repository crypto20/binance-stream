package org.clebi.binancespark;

import com.google.auto.value.AutoValue;
import java.io.Serializable;

@AutoValue
abstract class TradeVwap implements Serializable {

  private static final long serialVersionUID = 4699894724481104371L;

  static TradeVwap create(String symbol, long time, double vwap) {
    return new AutoValue_TradeVwap(symbol, time, vwap);
  }

  abstract String symbol();

  abstract long time();

  abstract double vwap();

}
