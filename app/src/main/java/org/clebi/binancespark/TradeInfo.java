package org.clebi.binancespark;

import com.google.auto.value.AutoValue;
import java.io.Serializable;

@AutoValue
abstract class TradeInfo implements Serializable {

  private static final long serialVersionUID = -1256699724345834276L;

  static TradeInfo create(long tradeTime, String symbol, double priceVolume, double quantity) {
    return new AutoValue_TradeInfo(tradeTime, symbol, priceVolume, quantity);
  }

  abstract long tradeTime();

  abstract String symbol();

  abstract double priceVolume();

  abstract double quantity();
}
