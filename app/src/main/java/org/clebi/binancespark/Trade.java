package org.clebi.binancespark;

import com.google.auto.value.AutoValue;
import java.io.Serializable;

@AutoValue
abstract class Trade implements Serializable {
  private static final long serialVersionUID = -6594527194815733812L;

  static Trade create(String eventType, long eventTime, String symbol, long tradeId, double price, double quantity,
      long buyerId, long sellerId, long tradeTime, boolean isMarketMaker) {
    return new AutoValue_Trade(
        eventType, 
        eventTime, 
        symbol, 
        tradeId, 
        price, 
        quantity, 
        buyerId, 
        sellerId, 
        tradeTime,
        isMarketMaker);
  }

  abstract String eventType();

  abstract long eventTime();

  abstract String symbol();

  abstract long tradeId();

  abstract double price();

  abstract double quantity();

  abstract long buyerId();

  abstract long sellerId();

  abstract long tradeTime();

  abstract boolean isMarketMaker();
}
